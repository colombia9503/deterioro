package co.creativosdigitales.jsf.dao;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import co.creativosdigitales.jsf.common.BaseXClient;
import co.creativosdigitales.jsf.common.BaseXClient.Query;

public class ImportDao {
	private static ImportDao instance;
	private String db = "deterioro";
	private String imports = "import module namespace imports = 'http://creativosdigitales.co/cartera/imports';";
	private Logger logger = Logger.getLogger(getClass().getName());
	
	public static ImportDao getInstance() {
		if(instance == null) {
			instance = new ImportDao();
		}
		return instance;
	}
	
	public ImportDao() {
	}
	
	public void addDocument(String filePath) throws IOException {
		try (BaseXClient session = new BaseXClient("localhost", 1984, "admin", "admin")){
			String query = imports
					+ "let $db := collection('"+db+"')"
					+ "let $csv := '"+filePath+"'"
					+ "return insert node imports:import-portfolio( $csv ) into $db/portfolio";
			Query xquery = session.query(query);
			xquery.execute();
			logger.log(Level.INFO, session.info());
			FacesMessage message = new FacesMessage("Archivo importado correctamente: ", "CSV importado correctamente!");
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}

}
