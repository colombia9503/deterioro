package co.creativosdigitales.jsf.dao;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import co.creativosdigitales.jsf.common.BaseXClient;
import co.creativosdigitales.jsf.common.BaseXClient.Query;
import co.creativosdigitales.jsf.datacore.Dao;
import co.creativosdigitales.jsf.pojos.extrafields.ExtraField;
import co.creativosdigitales.jsf.pojos.extrafields.ExtraFields;
import co.creativosdigitales.jsf.pojos.policies.Policy;
import co.creativosdigitales.jsf.pojos.portfolio.Asset;
import co.creativosdigitales.jsf.pojos.portfolio.Portfolio;
import co.creativosdigitales.jsf.pojos.simulations.Simulation;
import co.creativosdigitales.jsf.pojos.simulations.Simulations;

public class AssetDao implements Dao<Asset>{
	public static AssetDao instance;
	private final BaseXClient session;
	private String db ="deterioro";
	private String assetsModule = "import module namespace asset='http://creativosdigitales.co/cartera/assets';";
	private String simulationsModule = "import module namespace simulation='http://creativosdigitales.co/cartera/simulations';";
	
	private Logger logger = Logger.getLogger(getClass().getName());
	
	public static AssetDao getInstance() throws IOException {
		if(instance == null) {
			instance = new AssetDao();
		}
		return instance;
	}
	
	public AssetDao() throws IOException{
		session = new BaseXClient("localhost", 1984, "admin", "admin");
	}

	@Override
	public void create(Asset object) throws JAXBException, IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Asset object) throws JAXBException, IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(String id) throws IOException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Asset> get() throws IOException, JAXBException {
		List<Asset> results = new ArrayList<Asset>();
		long start = System.currentTimeMillis(); 
		try{
			session.execute("open "+db);
			final String xquery = "<portfolio> {for $data in subsequence(//asset, 1, 150) return <asset> {$data/assetid} {$data/payment} {$data/pendingpayments} <balance>{ format-number($data/balance, \"#.00\") }</balance> <npv>{ format-number(sum(//simulation[assetid=$data/assetid]/payment/npv), \"#.00\") }</npv> <difference>{ format-number($data/balance - sum(//simulation[assetid=$data/assetid]/payment/npv), \"#.00\") }</difference> </asset>} </portfolio>";
			JAXBContext context = JAXBContext.newInstance(Portfolio.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			Query query = session.query(xquery);
			Portfolio portfolio = (Portfolio) unmarshaller.unmarshal(new StringReader(query.execute()));
			return portfolio.getAssets();
		} finally {
			logger.log(Level.INFO, session.info());
		}
	}

	@Override
	public Asset getById(String id) throws JAXBException, IOException {
		Asset asset = null;
		return asset;
	}

	@Override
	public boolean exists(Asset object) {
		// TODO Auto-generated method stub
		return false;
	}
	
	//custom methods
	public List<ExtraField> getExtraFields(String assetId) throws IOException, JAXBException {
		long start = System.currentTimeMillis(); 
		try {
			final String xquery = assetsModule
					+ "let $db := collection('"+db+"')"
					+ "let $asset := asset:asset-info($db, '"+assetId+"')"
					+ "return asset:extra-fields($asset)";
			JAXBContext context = JAXBContext.newInstance(ExtraFields.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			Query query = session.query(xquery);
			ExtraFields extrafields = (ExtraFields) unmarshaller.unmarshal(new StringReader(query.execute()));
			return extrafields.getExtrafields();
		} finally {
			logger.log(Level.INFO, session.info());
		}
	}
	
	public List<Simulation> getSimulation(String assetId) throws IOException, JAXBException {
		long start = System.currentTimeMillis(); 
		try {
			final String xquery = assetsModule
					+ simulationsModule
					+ "let $db := collection('"+db+"')"
					+ "let $asset := asset:asset-info($db, '"+assetId+"')"
					+ "return simulation:simulation-xml($db, $asset)";
			JAXBContext context = JAXBContext.newInstance(Simulations.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			Query query = session.query(xquery);
			Simulations simulations = (Simulations) unmarshaller.unmarshal(new StringReader(query.execute()));
			return simulations.getSimulations();
		} finally {
			logger.log(Level.INFO, session.info());
		}
	}

}
