package co.creativosdigitales.jsf.dao;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import co.creativosdigitales.jsf.common.BaseXClient;
import co.creativosdigitales.jsf.common.BaseXClient.Query;
import co.creativosdigitales.jsf.datacore.Dao;
import co.creativosdigitales.jsf.pojos.policies.Policy;

public class PolicyDao implements Dao<Policy>{
	private static PolicyDao instance;
	private String db ="deterioro";
	
	
	public static PolicyDao getInstance() {
		if(instance == null) {
			instance = new PolicyDao();
		}
		return instance;
	}
	
	public PolicyDao() {
		
	}

	@Override
	public void create(Policy object) throws JAXBException, IOException {
		try (BaseXClient session = new BaseXClient("localhost", 1984, "admin", "admin")){
			InputStream bais = new ByteArrayInputStream(object.parseXml().getBytes());
			session.execute("open "+db);
			session.add("/policies/"+object.getId()+".xml", bais);
		} 
	}

	@Override
	public void update(Policy object) throws JAXBException, IOException {
		try (BaseXClient session = new BaseXClient("localhost", 1984, "admin", "admin")){
			InputStream bais = new ByteArrayInputStream(object.parseXml().getBytes());
			session.execute("open "+db);
			session.replace("policies/"+object.getId()+".xml", bais);
		} 
	}

	@Override
	public void delete(String id) throws IOException {
		try (BaseXClient session = new BaseXClient("localhost", 1984, "admin", "admin")){
			session.execute("open "+db);
			session.execute("delete \"policies/"+id+".xml\"");
		}
	}

	@Override
	public List<Policy> get() throws JAXBException, IOException {
		List<Policy> results = new ArrayList<Policy>();
		try(BaseXClient session = new BaseXClient("localhost", 1984, "admin", "admin")){
			session.execute("open "+db);
			final String xquery = "//policy";
			JAXBContext context = JAXBContext.newInstance(Policy.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			Query query = session.query(xquery);
			while(query.more()) {
				results.add((Policy) unmarshaller.unmarshal(new StringReader(query.next())));
			}
			return results;
		}
	}

	@Override
	public Policy getById(String id) throws JAXBException, IOException {
		Policy obj = null;
		try (BaseXClient session = new BaseXClient("localhost", 1984, "admin", "admin")){
			session.execute("open "+db);
			final String xquery = "//policy[id="+id+"]";
			JAXBContext context = JAXBContext.newInstance(Policy.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			Query query = session.query(xquery);
			while(query.more()) {
				obj = (Policy) unmarshaller.unmarshal(new StringReader(query.next()));
			}
		} 
		return obj;
	}

	@Override
	public boolean exists(Policy object) {
		// TODO Auto-generated method stub
		return false;
	}
	

}
