package co.creativosdigitales.jsf.datacore;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBException;

public interface Dao<E> {
	public void create(E object) throws JAXBException, IOException;
	public void update(E object) throws JAXBException, IOException;
	public void delete(String id) throws IOException;
	public List<E> get() throws IOException, JAXBException;
	public E getById(String id) throws JAXBException, IOException;
	public boolean exists(E object);
}
