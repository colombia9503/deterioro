package co.creativosdigitales.jsf.pojos.portfolio;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@ManagedBean
@XmlRootElement(name="portfolio")
@XmlAccessorType(XmlAccessType.FIELD)
public class Portfolio {
	@XmlElement(name="asset")
	private List<Asset> assets = null;

	public List<Asset> getAssets() {
		return assets;
	}

	public void setAssets(List<Asset> assets) {
		this.assets = assets;
	}
}
