package co.creativosdigitales.jsf.pojos.portfolio;

import javax.faces.bean.ManagedBean;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@ManagedBean
@XmlRootElement(name="asset")
@XmlAccessorType(XmlAccessType.FIELD)
public class Asset {
	private String assetid;
	private String payment;
	private String pendingpayments;
	private String balance;
	//net present value
	private String npv;
	//difference between balance and npv
	private String difference; 
	
	public Asset() {
		// TODO Auto-generated constructor stub
	}

	public String getAssetid() {
		return assetid;
	}

	public void setAssetid(String assetid) {
		this.assetid = assetid;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getPendingpayments() {
		return pendingpayments;
	}

	public void setPendingpayments(String pendingpayments) {
		this.pendingpayments = pendingpayments;
	}

	public String getBalance() {
		return balance;
	}

	public void setBalance(String balance) {
		this.balance = balance;
	}

	public String getNpv() {
		return npv;
	}

	public void setNpv(String npv) {
		this.npv = npv;
	}

	public String getDifference() {
		return difference;
	}

	public void setDifference(String difference) {
		this.difference = difference;
	}
}
