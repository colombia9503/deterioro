package co.creativosdigitales.jsf.pojos.simulations;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@ManagedBean
@XmlRootElement(name="simulations")
@XmlAccessorType(XmlAccessType.FIELD)
public class Simulations {
	@XmlElement(name="simulation")
	private List<Simulation> simulations;

	public List<Simulation> getSimulations() {
		return simulations;
	}

	public void setSimulations(List<Simulation> simulations) {
		this.simulations = simulations;
	}
}
