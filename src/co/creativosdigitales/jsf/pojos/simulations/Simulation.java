package co.creativosdigitales.jsf.pojos.simulations;

import javax.faces.bean.ManagedBean;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@ManagedBean
@XmlRootElement(name="simulation")
@XmlAccessorType(XmlAccessType.FIELD)
public class Simulation {
	private String number;
	private String principal;
	private String interest;
	private String newbalance;
	private String total;
	private String npv;
	private String punish;
	
	public Simulation() {
		// TODO Auto-generated constructor stub
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getPrincipal() {
		return principal;
	}

	public void setPrincipal(String principal) {
		this.principal = principal;
	}

	public String getInterest() {
		return interest;
	}

	public void setInterest(String interest) {
		this.interest = interest;
	}

	public String getNewbalance() {
		return newbalance;
	}

	public void setNewbalance(String newbalance) {
		this.newbalance = newbalance;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getNpv() {
		return npv;
	}

	public void setNpv(String npv) {
		this.npv = npv;
	}

	public String getPunish() {
		return punish;
	}

	public void setPunish(String punish) {
		this.punish = punish;
	}
}
