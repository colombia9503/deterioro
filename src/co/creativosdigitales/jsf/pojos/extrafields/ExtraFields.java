package co.creativosdigitales.jsf.pojos.extrafields;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@ManagedBean
@XmlRootElement(name="extrafields")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExtraFields {
	@XmlElement(name="extrafield")
	private List<ExtraField> extrafields;

	public List<ExtraField> getExtrafields() {
		return extrafields;
	}

	public void setExtrafields(List<ExtraField> extrafields) {
		this.extrafields = extrafields;
	}
}
