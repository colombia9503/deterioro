package co.creativosdigitales.jsf.pojos.extrafields;

import javax.faces.bean.ManagedBean;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@ManagedBean
@XmlRootElement(name="extrafield")
@XmlAccessorType(XmlAccessType.FIELD)
public class ExtraField {
	private String name;
	private String text;
	
	public ExtraField() {
		// TODO Auto-generated constructor stub
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
