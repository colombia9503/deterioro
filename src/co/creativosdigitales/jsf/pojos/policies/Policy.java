package co.creativosdigitales.jsf.pojos.policies;

import java.io.StringWriter;

import javax.faces.bean.ManagedBean;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@ManagedBean
@XmlRootElement(name = "policy")
@XmlType(propOrder = {"id", "name", "priority", "rate", "formula"})
public class Policy {
	private String id;
	private String name;
	private String priority;
	private String rate;
	private String formula;
	
	public Policy() {
		// TODO Auto-generated constructor stub
	}
	
	public Policy(String id, String name, String priority, String rate, String formula) {
		this.id = id;
		this.name = name;
		this.priority = priority;
		this.rate = rate;
		this.formula = formula;
	}

	@XmlElement(name = "id")
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	@XmlElement(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@XmlElement(name = "priority")
	public String getPriority() {
		return priority;
	}

	public void setPriority(String priority) {
		this.priority = priority;
	}
	
	@XmlElement(name = "rate")
	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	@XmlElement(name = "formula")
	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}
	
	public String parseXml() throws JAXBException {
		JAXBContext context = JAXBContext.newInstance(Policy.class);
		Marshaller marshaller = context.createMarshaller(); 
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		
		StringWriter sw = new StringWriter();
		marshaller.marshal(this, sw);
		return sw.toString();
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "["+id+"/"+name+"/"+priority+"/"+rate+"/"+formula+"]";
	}
}
