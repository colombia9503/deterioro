package co.creativosdigitales.jsf.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.xml.bind.JAXBException;

import co.creativosdigitales.jsf.dao.AssetDao;
import co.creativosdigitales.jsf.pojos.extrafields.ExtraField;
import co.creativosdigitales.jsf.pojos.portfolio.Asset;
import co.creativosdigitales.jsf.pojos.simulations.Simulation;
import net.bootsfaces.utils.FacesMessages;

@ManagedBean
@SessionScoped
public class IndexController {
	private List<Asset> assets;
	private List<String> assetids;
	//asset simulation
	private List<Simulation> simulation;
	//asset extrafields
	private List<ExtraField> extraFields;
	
	private Asset asset = new Asset();
	
	private AssetDao assetDao;
	private Logger logger = Logger.getLogger(getClass().getName());
	
	public IndexController() {
		try {
			assetDao = AssetDao.getInstance();
		} catch (IOException ex) {
			logger.log(Level.SEVERE, "Error connecting to BaseX IO", ex);
			FacesMessages.error(ex.getMessage());
		}
		assets = new ArrayList<Asset>();
		assetids = new ArrayList<String>();
	}

	public List<Asset> getAssets() {
		return assets;
	}

	public List<String> getAssetids() {
		return assetids;
	}
	
	
	
	public void loadAssets() {
		logger.info("loading assets..");
		try {
			assets = assetDao.get();
		}
		catch (JAXBException e) {
			logger.log(Level.SEVERE, "Error loading assets JAXB", e);
			FacesMessages.error(e.getMessage());
		}
		catch (IOException e) {
			logger.log(Level.SEVERE, "Error loading assets IO", e);
			FacesMessages.error(e.getMessage());
		}
	}
	
	public void loadAsset(Asset asset) {
		this.asset = asset;
		try {
			logger.info("loading simulations for.."+asset.getAssetid());
			simulation = assetDao.getSimulation(asset.getAssetid());
			
			logger.info("loading extrafields for.."+asset.getAssetid());
			extraFields = assetDao.getExtraFields(asset.getAssetid());
		}
		catch (JAXBException e) {
			logger.log(Level.SEVERE, "Error loading assets JAXB", e);
			FacesMessages.error(e.getMessage());
		}
		catch (IOException e) {
			logger.log(Level.SEVERE, "Error loading assets IO", e);
			FacesMessages.error(e.getMessage());
		}
	}

	public Asset getAsset() {
		return asset;
	}

	public void setAsset(Asset asset) {
		this.asset = asset;
	}

	public List<Simulation> getSimulation() {
		return simulation;
	}

	public List<ExtraField> getExtraFields() {
		return extraFields;
	}
	
}
