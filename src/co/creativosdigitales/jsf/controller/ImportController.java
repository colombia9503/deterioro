package co.creativosdigitales.jsf.controller;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.myfaces.custom.fileupload.UploadedFile;

import co.creativosdigitales.jsf.dao.ImportDao;

@ManagedBean
@SessionScoped
public class ImportController {
	private UploadedFile uploadedFile;
	private ImportDao importDao;
 	
	public ImportController() {
		importDao = ImportDao.getInstance();
	}

	public void submit() throws IOException {
        String fileName = FilenameUtils.getName(uploadedFile.getName());
        String contentType = uploadedFile.getContentType();
        byte[] bytes = uploadedFile.getBytes();
        //save generating uuid
        UUID fileIdentifier = UUID.randomUUID();
        String filePath = "C:/det_cartera/files/" + fileIdentifier + "/" + fileName;
        FileUtils.writeByteArrayToFile(new File(filePath), bytes);
        //importDao.addDocument(filePath);
        
        FacesMessage message = new FacesMessage("Archivo subido correctamente:", "Archivo "+uploadedFile.getName());
		FacesContext.getCurrentInstance().addMessage(null, message);
    }

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}
}
	
