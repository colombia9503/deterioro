package co.creativosdigitales.jsf.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.xml.bind.JAXBException;

import co.creativosdigitales.jsf.dao.PolicyDao;
import co.creativosdigitales.jsf.pojos.policies.Policy;
import net.bootsfaces.utils.FacesMessages;

@ManagedBean
@SessionScoped
public class PoliciesController {
	private List<Policy> policies;
	private PolicyDao policyDao;
	private Logger logger = Logger.getLogger(getClass().getName());
	private Policy edit = new Policy();
	
	public PoliciesController() {
		policyDao = PolicyDao.getInstance();
		policies = new ArrayList<Policy>();
	}

	public List<Policy> getPolicies() {
		return policies;
	}

	public void loadPolicies() {
		logger.info("loading policies..");
		try {
			policies = policyDao.get();
		}
		catch (JAXBException e) {
			logger.log(Level.SEVERE, "Error loading policies", e);
			addErrorMessage(e);
		}
		catch (IOException e) {
			logger.log(Level.SEVERE, "Error loading policies", e);
			addErrorMessage(e);
		}
	}
	
	public void loadPolicy(Policy policy) {
		logger.info("loading policy: "+policy.toString());
		this.edit = policy;
	}
	
	public String addPolicy(Policy policy) {
		logger.info("creating policy: " + policy.toString());
		try {
			policyDao.create(policy);
		}
		catch (JAXBException e) {
			logger.log(Level.SEVERE, "Error creating policies", e);
			addErrorMessage(e);
		}
		catch (IOException e) {
			logger.log(Level.SEVERE, "Error creating policies", e);
			addErrorMessage(e);
		}
		finally {
			FacesMessages.info("Pol�tica guardada correctamente!");
		}
		return "policies";
	}
	
	public String updatePolicy(Policy policy) {
		try {
			policyDao.update(policy);
		}
		catch (JAXBException e) {
			logger.log(Level.SEVERE, "Error updating policies", e);
			addErrorMessage(e);
		}
		catch (IOException e) {
			logger.log(Level.SEVERE, "Error updating policies", e);
			addErrorMessage(e);		
		}
		finally {
			FacesMessages.info("Pol�tica editada correctamente!");
		}
		return "policies";
	}
	
	public String deletePolicy(String id) {
		logger.info("deleting policy: "+id);
		try {
			policyDao.delete(id);
		}
		catch (IOException e) {
			logger.log(Level.SEVERE, "Error deleting policy: ", e);
		}
		finally {
			FacesMessages.info("Pol�tica eliminada correctamente!");
		}
		return "policies";
	}
	
	private void addErrorMessage(Exception exc) {
		FacesMessage message = new FacesMessage("Error: " + exc.getMessage());
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	public Policy getEdit() {
		return edit;
	}
	
}
